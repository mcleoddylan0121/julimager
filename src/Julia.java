public class Julia implements Drawable {
	private Complex constant;
	static final int MAX_ITER_COLOR = 10;
	static final int MAX_ITER = 40;
	public static final int IN_SET = -1;
	public static final ColorMap color = ColorMap.colorcolor;

	public Julia(Complex constant) {
		this.constant = constant;
	}

	public Julia(double real, double imaginary) {
		this(new Complex(real, imaginary));
	}

	public Complex getConstant() {
		return constant;
	}

	public boolean contains(Complex c) {
		return escapeTime(c) == IN_SET;
	}

	public int escapeTime(Complex c) {
		for (int i = 0; i < MAX_ITER; i++) {
			if (c.absSquare() > 4) return i;
			c = iterate(c);
		}
		return IN_SET;
	}

	public int getColor(Complex c) {
		double col = Math.exp(-c.abs());
		for (int i = 0; i < MAX_ITER_COLOR && c.abs() < 30; i++) {
			c = iterate(c);
			col += Math.exp(-c.abs());
		}
		return color.getColor(col / MAX_ITER_COLOR);
	}

	public int getColorOld(Complex c) {
		int i = (escapeTime(c));
		if (i == IN_SET) return 0;
		i = (i * 255 / MAX_ITER);
		return (i << 16) + (i << 8) + i;
	}

	public Complex iterate(Complex c) {
		return c.square().add(constant);
	}

	@Override
	public void getPixels(double x, double y, int width, int height, double xstep, double ystep, int[] pixels) {
		Complex start = new Complex(x - width * xstep / 2, y - height * ystep / 2);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height / 2; j++) {
				pixels[i + j * width] = getColor(start.add(new Complex(i * xstep, j * xstep)));
				// Rotate around origin by 180 degrees, since quadratic julia sets have rotational symmetry
				pixels[width - i - 1 + (height - j - 1) * width] = pixels[i + j * width];
			}
		}
	}

	/** sets the constant to the given complex */
	public void setConst(Complex c) {
		constant = c;
	}
}
