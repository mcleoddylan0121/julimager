public class Complex {
	public static final Complex ORIGIN = new Complex(0, 0);
	private double real, imaginary;

	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	public Complex add(Complex c) {
		return new Complex(real + c.real, imaginary + c.imaginary);
	}

	public Complex subtract(Complex c) {
		return new Complex(real - c.real, imaginary - c.imaginary);
	}

	public Complex square() {
		return multiply(this);
	}

	public Complex multiply(Complex c) {
		return new Complex(-imaginary * c.imaginary + real * c.real, real * c.imaginary + imaginary * c.real);
	}

	/** this is too cool */
	public Complex divide(Complex c) {
		// Note that the absSquare comes from multiplying the numerator and denominator by the conjugate of c
		return (multiply(c.conjugate())).divide(c.absSquare());
	}

	public Complex divide(double d) {
		return new Complex(real / d, imaginary / d);
	}

	public Complex conjugate() {
		return new Complex(real, -imaginary);
	}

	public double abs() {
		return Math.sqrt(real * real + imaginary * imaginary);
	}

	/**
	 * gives the absolute value squared, useful when multiplying by the
	 * conjugate
	 */
	public double absSquare() {
		return real * real + imaginary * imaginary;
	}

	@Override
	public String toString() {
		return real + " + " + imaginary + "i";
	}

	public double real() {
		return real;
	}

	public double imaginary() {
		return imaginary;
	}

	@Override
	public Complex clone() {
		return new Complex(real, imaginary);
	}
}
