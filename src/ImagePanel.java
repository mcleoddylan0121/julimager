import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JPanel;

public class ImagePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	BufferedImage image;
	int[] pixels;
	int width = 800;
	int height = 800;
	Drawable source;
	double sourceX, sourceY, sourceStepX, sourceStepY;
	int sourceWidth, sourceHeight;

	/** Holds the specified drawable */
	public ImagePanel(Drawable source, int width, int height) {
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		this.width = width;
		this.height = height;
		this.source = source;
		sourceX = 0;
		sourceY = 0;
		sourceWidth = width;
		sourceHeight = height;
		sourceStepX = 0.005;
		sourceStepY = 0.005;
		refresh();
		setVisible(true);
	}

	public void refresh() {
		setPixels();
		repaint();
	}

	public void setPixels() {
		source.getPixels(sourceX, sourceY, sourceWidth, sourceHeight, sourceStepX, sourceStepY, pixels);
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(image, 0, 0, getSize().width, getSize().height, 0, 0, width, height, null);
	}

	public void wrap(Drawable d) {
		source = d;
	}
}
