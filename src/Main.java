import java.awt.MouseInfo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Main implements MouseListener {
	public static Main temp;
	public boolean mandel = false;
	public Mandelbrot Mandel = new Mandelbrot(new Complex(0, 0));
	int WIDTH = 800;
	int HEIGHT = 600;
	public Julia jul = new Julia(0, 0);
	public ImagePanel img = new ImagePanel(jul, WIDTH, HEIGHT);
	public Window win = new Window(WIDTH, HEIGHT);

	public Main() {
		win.add(img);
		win.pack();
		win.repaint();
		win.addMouseListener(this);
		double x = 0, y = 0;
		while (true) {
			if (MouseInfo.getPointerInfo().getLocation().x == x && MouseInfo.getPointerInfo().getLocation().y == y) continue;
			x = 3 * ((double) (MouseInfo.getPointerInfo().getLocation().x - win.getX()) - win.getWidth() / 2) / win.getWidth();
			y = 3 * ((double) (MouseInfo.getPointerInfo().getLocation().y - win.getY()) - win.getHeight() / 2) / win.getHeight();
			Complex c = new Complex(x, y);
			jul.setConst(c);
			Mandel.setConst(c);
			img.refresh();
			win.repaint();
		}
	}

	public static void main(String[] args) {
		temp = new Main();
	}

	public void change() {
		mandel = !mandel;
		if (mandel) img.wrap(Mandel);
		else img.wrap(jul);
		img.refresh();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		change();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
