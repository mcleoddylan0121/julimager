import java.awt.Color;

public interface ColorMap {
	/**
	 * @param val A double between 0 and 1
	 * @return an RBG color mapped to the
	 */
	public int getColor(double val);

	public static final ColorMap colorful = new ColorMap() {

		@Override
		public int getColor(double val) {
			return Color.HSBtoRGB((float) val, 0.6f, 1.0f);
		}
	};

	public static final ColorMap grayScale = new ColorMap() {

		@Override
		public int getColor(double val) {
			if (val >= 1) val = 1;
			int i = (int) ((1 - val) * 255);
			return (i << 16) + (i << 8) + (i);
		}
	};

	public static final ColorMap colorcolor = new ColorMap() {
		@Override
		public int getColor(double val) {
			return Color.HSBtoRGB(0.3f + (float) val / 2, 0.6f, 1.0f);
		}
	};
}
