public interface Drawable {

	void getPixels(double x, double y, int width, int height, double xstep, double ystep, int[] pixels);
}
