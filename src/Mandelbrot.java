public class Mandelbrot implements Drawable {
	public static final int MAX_ITER_COLOR = 100;
	public static final int MAX_ITER = 100;
	public static final int IN_SET = -1;
	private Complex constant = new Complex(0, 0);
	public static final ColorMap color = ColorMap.colorful;

	public Mandelbrot(Complex constant) {
		this.constant = constant;
	}

	public Complex getConst() {
		return constant;
	}

	public void setConst(Complex val) {
		constant = val;
	}

	public boolean contains(Complex c) {
		return escapeTime(c) == Julia.IN_SET;
	}

	public int getColor(Complex c) {
		Complex zn = constant.clone();
		int n;
		for (n = 0; n < MAX_ITER_COLOR; n++) {
			if (zn.absSquare() > 4) break;
			zn = iterate(zn, c);
		}
		double col = n + 1 - Math.log(Math.log(zn.abs())) / Math.log(2);
		return color.getColor(col / MAX_ITER_COLOR);
	}

	public int getColorOld(Complex c) {
		int i = escapeTime(c);
		if (i == IN_SET) return 0;
		i = (i * 255 / MAX_ITER);
		return (i << 16) + (i << 8) + i;
	}

	private int escapeTime(Complex c) {
		Complex temp = constant.clone();
		for (int i = 0; i < MAX_ITER; i++) {
			if (temp.absSquare() > 4) return i;
			temp = iterate(temp, c);
		}
		return Julia.IN_SET;
	}

	private Complex iterate(Complex a, Complex b) {
		return a.square().add(b);
	}

	@Override
	public void getPixels(double x, double y, int width, int height, double xstep, double ystep, int[] pixels) {
		Complex start = new Complex(x - width * xstep / 2 - 0.5, y - height * ystep / 2);
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++) {
				pixels[i + j * width] = getColor(start.add(new Complex(i * xstep, j * xstep)));
			}
	}
}
